﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState { idle, walk, attack }

public class PlayerManager : MonoBehaviour
{

    public PlayerState currentState;

    PlayerMovement pm;

    [Header("Movement Variables")]
    [HideInInspector] public Vector2 leftStick = new Vector2(0, 0);
    [HideInInspector] public Vector2 rightStick = new Vector2(0, 0);
    [HideInInspector] public Vector2 lastLookDirection = new Vector2(0, -1);
    //public string lastLookDirectionDefined = "South";
    public bool isRunning = false;
    public float moveSpeed = 5f;
    public float runSpeedMultiplier = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        pm = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        UnityDefaultInput();
    }

    private void UnityDefaultInput()
    {
        leftStick.x = Input.GetAxisRaw("Horizontal");
        leftStick.y = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Call attack here
            print("Space key has been registered");
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            // Call restart here?
            print("Return key has been registered");
        }
    }

    // Use this for physics updates.
    private void FixedUpdate()
    {
        // Player movement called here.
        if (currentState == PlayerState.idle || currentState == PlayerState.walk)
        {   
            pm.movement = leftStick;
            //pm.lookDirection = rightStick;            

            // I'm sure there's a better way to do this
            if (lastLookDirection.x != 0 || lastLookDirection.y != 0)
                pm.lastLookDirection = lastLookDirection;

            //pm.isRunning = isRunning;
            pm.moveSpeed = moveSpeed;
            //pm.runSpeedMultiplier = runSpeedMultiplier;
            pm.PlayerMove();

            // Rotates attack point
            //Rotate();
        }
    }
}
